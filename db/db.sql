CREATE TABLE movie (
  movie_id INTEGER PRIMARY KEY auto_increment,
  movie_title VARCHAR(100),
  movie_release_date DATE,
  movie_time TIME,
  director_name VARCHAR(100)
);


INSERT INTO movie (movie_title, movie_release_date, movie_time, director_name) VALUES ('3 Idiots ', '2012-01-01','20:50:12', 'piyush');
INSERT INTO movie (movie_title, movie_release_date, movie_time, director_name) VALUES ('Dhoom', '2012-01-01', '20:50:12',  'piyush');